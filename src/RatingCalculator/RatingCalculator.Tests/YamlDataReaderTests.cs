﻿using RatingCalculator.Calculator.Readers;
using RatingCalculator.Calculator.Types;
using RatingCalculator.Tests.Helpers;

namespace RatingCalculator.Tests;

public class YamlDataReaderTests
{
    private readonly string _text;
    private readonly DataType _data;
    public YamlDataReaderTests()
    {
        _text = "- Иванов Константин Дмитриевич\n   математика: 91\n    химия: 100\n- Петров Петр Семенович\n    русский язык: 87\n    литература: 78\n";
        _data = new DataType
        {
            Data = new Dictionary<string, List<SubjectInfo>>()
            {
                {
                    "Иванов Константин Дмитриевич", new List<SubjectInfo>()
                    {
                        new("математика", 91),
                        new("химия", 100)
                    }
                },
                {
                    "Петров Петр Семенович", new List<SubjectInfo>()
                    {
                        new("русский язык", 87),
                        new("литература", 78)
                    }
                }
            }
        };
    }
    
    [Fact]
    public void DataTypeReadSuccessfullyTest()
    {
        Directory.CreateDirectory("test");
        using(var writer = new StreamWriter(File.Create("test/test.yaml")))
        {
            writer.Write(_text);
        }

        var txtReader = new YamlDataReader();
        var data = txtReader.Read("test/test.yaml");
        Assert.Equal(_data, data, new DataTypeComparer());
    }
    [Fact]
    public void ReaderThrowsException()
    {
        Assert.Throws<FileNotFoundException>(() =>
        {
            var txtReader = new YamlDataReader();
            var data = txtReader.Read("file");
        });
    }
    
    [Fact]
    public void ReaderThrowsExceptionWhenFormatIsInvalid()
    {
        var invalidFormat = " Иванов Константин Дмитриевич\n   математика: 91\n    химия: 100\n- Петров Петр Семенович\n   русский язык: 87\n  литература: 78\n";
        
        Directory.CreateDirectory("test");
        using(var writer = new StreamWriter(File.Create("test/invalid_format.yaml")))
        {
            writer.Write(invalidFormat);
        }
        
        Assert.Throws<ArgumentException>(() =>
        {
            var txtReader = new YamlDataReader();
            var data = txtReader.Read("test/invalid_format.yaml");
        });
    }
    [Fact]
    public void ReaderThrowsExceptionWhenSeparatorNotFound()
    {
        var invalidFormat = "- Иванов Константин Дмитриевич\n   математика 91\n    химия 100\n- Петров Петр Семенович\n   русский язык: 87\n    литература: 78\n";
        
        Directory.CreateDirectory("test");
        using(var writer = new StreamWriter(File.Create("test/invalid_separator.yaml")))
        {
            writer.Write(invalidFormat);
        }
        
        Assert.Throws<ArgumentException>(() =>
        {
            var txtReader = new YamlDataReader();
            var data = txtReader.Read("test/invalid_separator.yaml");
        });
    }
}