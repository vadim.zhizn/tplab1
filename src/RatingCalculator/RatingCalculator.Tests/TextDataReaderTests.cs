using RatingCalculator.Calculator.Readers;
using RatingCalculator.Calculator.Types;
using RatingCalculator.Tests.Helpers;

namespace RatingCalculator.Tests;

public class TextDataReaderTests
{
    private readonly string _text;
    private readonly DataType _data;
    public TextDataReaderTests()
    {
        _text = "Иванов Константин Дмитриевич\n математика:91\n химия:100\nПетров Петр Семенович\n русский язык:87\n литература:78\n";
        _data = new DataType
        {
            Data = new Dictionary<string, List<SubjectInfo>>()
            {
                {
                    "Иванов Константин Дмитриевич", new List<SubjectInfo>()
                    {
                        new("математика", 91),
                        new("химия", 100)
                    }
                },
                {
                    "Петров Петр Семенович", new List<SubjectInfo>()
                    {
                        new("русский язык", 87),
                        new("литература", 78)
                    }
                }
            }
        };
    }
    
    [Fact]
    public void DataTypeReadSuccessfullyTest()
    {
        Directory.CreateDirectory("test");
        using(var writer = new StreamWriter(File.Create("test/test.txt")))
        {
            writer.Write(_text);
        }

        var txtReader = new TextDataReader();
        var data = txtReader.Read("test/test.txt");
        Assert.Equal(_data, data, new DataTypeComparer());
    }
    [Fact]
    public void ReaderThrowsException()
    {
        Assert.Throws<FileNotFoundException>(() =>
        {
            var txtReader = new TextDataReader();
            var data = txtReader.Read("file");
        });
    }
}