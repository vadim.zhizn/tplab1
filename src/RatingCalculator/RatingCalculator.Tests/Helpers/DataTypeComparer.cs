﻿using RatingCalculator.Calculator.Types;

namespace RatingCalculator.Tests.Helpers;

public class DataTypeComparer : IEqualityComparer<DataType>
{
    public bool Equals(DataType x, DataType y)
    {
        if (ReferenceEquals(x, y)) return true;
        if (ReferenceEquals(x, null)) return false;
        if (ReferenceEquals(y, null)) return false;
        if (x.GetType() != y.GetType()) return false;
        if (x.Data.Keys.Count != y.Data.Keys.Count) return false;
        foreach (var key in x.Data.Keys)
        {
            if (y.Data.TryGetValue(key, out var value))
            {
                if (x.Data[key].Count != value.Count) return false;
                
                for (int i = 0; i < value.Count; i++)
                {
                    var xVal = x.Data[key][i];
                    var yVal = value[i];
                    if (xVal.Score != yVal.Score || xVal.SubjectName != yVal.SubjectName)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    public int GetHashCode(DataType obj)
    {
        return obj.Data.GetHashCode();
    }
}