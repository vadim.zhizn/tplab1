﻿using RatingCalculator.Calculator;
using RatingCalculator.Calculator.Types;

namespace RatingCalculator.Tests;

public class CalculationTests
{
    [Fact]
    public void StudentNotFound()
    {
        var data = new DataType
        {
            Data = new Dictionary<string, List<SubjectInfo>>()
            {
                {
                    "Иванов Константин Дмитриевич", new List<SubjectInfo>()
                    {
                        new("математика", 91),
                        new("химия", 100),
                        new("биология", 67)
                    }
                },
                {
                    "Петров Петр Семенович", new List<SubjectInfo>()
                    {
                        new("русский язык", 87),
                        new("литература", 78)
                    }
                }
            }
        };

        var calc = new CalcRating(data);
        var result = calc.GetUserWithScore76GreaterOrEqualThen3Subject();
        Assert.Null(result);
    }
    
    [Fact]
    public void StudentFound()
    {
        var data = new DataType
        {
            Data = new Dictionary<string, List<SubjectInfo>>()
            {
                {
                    "Иванов Константин Дмитриевич", new List<SubjectInfo>()
                    {
                        new("математика", 91),
                        new("химия", 100),
                        new("биология", 78)
                    }
                },
                {
                    "Петров Петр Семенович", new List<SubjectInfo>()
                    {
                        new("русский язык", 87),
                        new("литература", 78)
                    }
                }
            }
        };

        var calc = new CalcRating(data);
        var result = calc.GetUserWithScore76GreaterOrEqualThen3Subject();
        Assert.Equal("Иванов Константин Дмитриевич", result);
    }
    
    [Fact]
    public void TwoStudentsFound()
    {
        var data = new DataType
        {
            Data = new Dictionary<string, List<SubjectInfo>>()
            {
                {
                    "Иванов Константин Дмитриевич", new List<SubjectInfo>()
                    {
                        new("математика", 91),
                        new("химия", 100),
                        new("биология", 78)
                    }
                },
                {
                    "Петров Петр Семенович", new List<SubjectInfo>()
                    {
                        new("русский язык", 87),
                        new("литература", 78),
                        new("география", 78)
                    }
                }
            }
        };

        var calc = new CalcRating(data);
        var result = calc.GetUserWithScore76GreaterOrEqualThen3Subject();
        Assert.Equal("Иванов Константин Дмитриевич", result);
    }
    [Fact]
    public void ThrowsException()
    {
        Assert.Throws<NullReferenceException>(() =>
        {
            var calc = new CalcRating(null);
            var result = calc.GetUserWithScore76GreaterOrEqualThen3Subject();
        });
    }
}