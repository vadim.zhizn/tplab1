﻿using System.Text;

namespace RatingCalculator.Calculator.Types;

public class DataType
{
    public Dictionary<string, List<SubjectInfo>> Data { get; set; }

    public DataType()
    {
        Data = new Dictionary<string, List<SubjectInfo>>();
    }

    public override string ToString()
    {
        var builder = new StringBuilder();
        foreach (var key in Data.Keys)
        {
            builder.Append(key);
            builder.Append(Environment.NewLine);
            builder.Append(string.Join("\n", Data[key]));
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);
        }
        return builder.ToString();
    }
}