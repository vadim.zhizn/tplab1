﻿namespace RatingCalculator.Calculator.Types;

public class SubjectInfo
{
    public int Score { get; set; }
    public string SubjectName { get; set; }

    public SubjectInfo(string subjectName, int score)
    {
        SubjectName = subjectName;
        Score = score;
    }

    public override string ToString()
    {
        return $"{SubjectName}:{Score}";
    }

}