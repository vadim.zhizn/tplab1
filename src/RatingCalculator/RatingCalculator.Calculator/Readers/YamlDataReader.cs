﻿using RatingCalculator.Calculator.Types;

namespace RatingCalculator.Calculator.Readers;

public class YamlDataReader : IDataReader
{
    public DataType Read(string path)
    {
        if (!File.Exists(path))
        {
            throw new FileNotFoundException($"File not found in path {path}");
        }

        path = Path.GetFullPath(path);
        
        var result = new DataType();
        using (var reader = new StreamReader(File.OpenRead(path)))
        {
            var key = "";
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line.StartsWith("-"))
                {
                    key = line.Replace("-", "").Replace(":", "").Trim();
                    result.Data[key] = new List<SubjectInfo>();
                }
                else if(line.StartsWith("   ") || line.StartsWith("\t"))
                {
                    var splitted = line.Split(':', 2);
                    if (!result.Data.TryGetValue(key, out _) || splitted.Length != 2)
                    {
                        throw new ArgumentException("Некорректный формат файла");
                    }
                    result.Data[key].Add(new SubjectInfo(splitted[0].Trim(), int.Parse(splitted[1])));
                }
                else if (line.Trim() != "")
                {
                    throw new ArgumentException("Некорректный формат файла");
                }
            }
        }

        return result;
    }
}