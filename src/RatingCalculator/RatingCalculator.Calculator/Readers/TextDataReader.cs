﻿using RatingCalculator.Calculator.Types;

namespace RatingCalculator.Calculator.Readers;

public class TextDataReader : IDataReader
{
    public DataType Read(string path)
    {
        if (!File.Exists(path))
        {
            throw new FileNotFoundException($"File not found in path {path}");
        }

        var result = new DataType();
        using (var reader = new StreamReader(File.OpenRead(path)))
        {
            var key = "";
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!line.StartsWith(" "))
                {
                    key = line.Trim();
                    result.Data[key] = new List<SubjectInfo>();
                }
                else
                {
                    var splitted = line.Split(':', 2);
                    result.Data[key].Add(new SubjectInfo(splitted[0].Trim(), int.Parse(splitted[1])));
                }
            }
        }

        return result;
    }
}