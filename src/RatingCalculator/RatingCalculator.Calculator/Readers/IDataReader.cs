﻿using RatingCalculator.Calculator.Types;

namespace RatingCalculator.Calculator.Readers;

public interface IDataReader
{
    DataType Read(string path);
}