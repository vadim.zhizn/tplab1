﻿using RatingCalculator.Calculator;
using RatingCalculator.Calculator.Readers;

if (args.Length < 2 || args[0] != "-p")
{
    Console.WriteLine("Укажите путь до файла в формате -p <path>");
    return;
}

var path = args[1];
if (!File.Exists(path))
{
    Console.WriteLine("Файл не найден");
    return;
}

var reader = new YamlDataReader();
var students = reader.Read(path);
Console.WriteLine("Students:");
Console.WriteLine(students);

var calc = new CalcRating(students);
var result = calc.GetUserWithScore76GreaterOrEqualThen3Subject();
Console.WriteLine("Rating:");
Console.WriteLine(result);
