﻿using RatingCalculator.Calculator.Types;

namespace RatingCalculator.Calculator;

public class CalcRating
{
    private readonly DataType _dataType;

    public CalcRating(DataType dataType) => _dataType = dataType;

    public Dictionary<string, double> Calculate()
    {
        var result = new Dictionary<string, double>();
        foreach (var key in _dataType.Data.Keys)
        {
            result[key] = 0.0;
            foreach (var subject in _dataType.Data[key])
            {
                result[key] += subject.Score;
            }

            result[key] /= _dataType.Data[key].Count;
        }
        return result;
    }

    public string GetUserWithScore76GreaterOrEqualThen3Subject()
    {
        if (_dataType is null)
        {
            throw new NullReferenceException();
        }

        if (_dataType.Data.Keys.Count == 0)
        {
            return null;
        }
        return _dataType.Data
            .Where(kvp => kvp.Value.Where(v => v.Score >= 76).Count() >= 3)
            .Select(kvp => kvp.Key)
            .FirstOrDefault();
    }
}